Link this folder for nvim configuration like so:

[source,zsh]
----
mkdir -p ~/.config/
ln -s ~/my-rcs/_config_nvim ~/.config/nvim
----
