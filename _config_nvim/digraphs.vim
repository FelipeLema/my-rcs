" I use these brackets a lot…
" ⎡
digraphs [[ 9121
" ⎦
digraphs ]] 9126
" re-write ⎡⋯⎦ → ⎡…⎦
digraphs .3 8230
" ♥
digraphs <3 9829
" ⨯ (vector cross product)
" You may want to use ^K (*X)
" to do ⎡×⎦
digraphs x- 10799
" ⇄
digraphs >< 8644

" ƒ
digraphs fn 402

" ㎚
digraphs nm 13210
" ㎛
digraphs um 13211
" ㎜
digraphs mm 13212
" ㎝
digraphs cm 13213
" ㎞
digraphs km 13214

" 🄯 (copy left)
" u.fsf.org/2lq
digraphs Cl 127279
