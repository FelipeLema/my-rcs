require "bit"
-- configure neovim using lua instead of vimscript
-- as much as possible

local set_vars = function(t)
	for k, v in pairs(t) do
		vim.api.nvim_set_var(k, tostring(v))
	end
end

-- re-map leader
-- http://learnvimscriptthehardway.stevelosh.com/chapters/06.html#leader
-- ↓ doesn't work as it's not «g:mapleader», «b:mapleader»
-- set_vars({mapleader=" "})
-- ∠ not an option
-- vim.api.nvim_set_option("mapleader", " ")
vim.api.nvim_command('let mapleader = " "')
-- leader keys
for key, comm in pairs({ 
    cn='cn',
    co='copen',
    cp='cp',
    q= 'qa', 
    s= 'w', 
    z= 'x' }) do
	vim.api.nvim_command(string.format('nnoremap <leader>%s :%s<CR>', key, comm))
end
-- Use backspace to enter command (like ⎡:⎦ does)
vim.api.nvim_command(string.format('nnoremap <BS> :', key, comm))


set_vars({python3_host_prog="python3"})


-- deoplete
set_vars({['deoplete#enable_at_startup']=1})

-- deoplete +  https://github.com/xolox/vim-lua-ftplugin
-- not working as expected (it's not using embedded lua, requires lua dist)
-- vim.api.nvim_call_function('deoplete#custom#var', {'omni', 'functions', {lua = 'xolox#lua#omnifunc'}})
-- local lua_deoplete_config =
-- {
-- 	lua_check_syntax = 0,
-- 	lua_complete_omni = 1,
-- 	lua_complete_dynamic = 0,
-- 	lua_define_completion_mappings = 0,
-- }
-- set_vars(lua_deoplete_config)
-- http://lua-users.org/wiki/LuaUnicode
-- translated thx to https://stackoverflow.com/a/40989497/3637404
-- http://bitop.luajit.org/
local Utf8to32 = function(utf8str)
	assert(type(utf8str) == "string")
	local res, seq, val = {}, 0, nil
	for i = 1, #utf8str do
		local c = string.byte(utf8str, i)
		if seq == 0 then
			table.insert(res, val)
			seq = c < 0x80 and 1 or c < 0xE0 and 2 or c < 0xF0 and 3 or
			c < 0xF8 and 4 or --c < 0xFC and 5 or c < 0xFE and 6 or
			error("invalid UTF-8 character sequence")
			val = bit.band(c, 2^(8-seq) - 1)
		else
			val = bit.bor(bit.lshift(val, 6), bit.band(c, 0x3F))
		end
		seq = seq - 1
	end
	table.insert(res, val)
	table.insert(res, 0)
	return res
end
local digraph = function(chars, unicode_char)
    local unicode_char_as_int = Utf8to32(unicode_char)[1]
    vim.api.nvim_command(string.format("digraph %s %d", chars, unicode_char_as_int))
end
for chars, dg in pairs({ 
	-- ⨯ (vector cross product)
	-- You may want to use ^K (*X) to do ⎡×⎦
	['x-'] = "⨯",
	['fn'] = "ƒ",
	['><'] = "⇄",
	['nm'] = "㎚",
	['um'] = "㎛",
	['mm'] = "㎜",
	['cm'] = "㎝",
	['km'] = "㎞",
	['[['] = "⎡",
	[']]'] = "⎦",
	['<3'] = "♥",
	['3!'] = "≢",
}) do
	digraph(chars, dg)
end
-- tab→4 spaces
-- https://stackoverflow.com/a/234578
for _,c in ipairs({ 
    "filetype plugin indent on",
    "filetype plugin indent on" ,
    "set tabstop=4",
    "set shiftwidth=4",
    "set expandtab" }) do
	vim.api.nvim_command(c)
end

-- https://stackoverflow.com/a/23535333
function _this_script_directory()
   local _str = debug.getinfo(2, "S").source:sub(2)
   return _str:match("(.*/)")
end

-- https://stackoverflow.com/a/4991602
function _file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

_local_settings_script = _this_script_directory() .. "local_settings.lua"
if _file_exists(_local_settings_script) then
    vim.api.nvim_command(string.format("luafile %s", _local_settings_script))
end

-- relative… numbering…?
vim.api.nvim_command("set relativenumber")
vim.api.nvim_command("set number")
vim.api.nvim_command("autocmd TermOpen * set norelativenumber")
vim.api.nvim_command("autocmd TermOpen * set nonumber")


-- preview replacement using ⎡:s/…⎦
vim.api.nvim_command("set inccommand=nosplit")
