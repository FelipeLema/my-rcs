" Quickly access this file like `:e $MYVIMRC` (see https://stackoverflow.com/a/10933797)
" Stuff here are meant to be in init.lua (will be gradually moving them so)

set background=dark
set termguicolors
packadd vim-afterglow
colo afterglow


" Clear search with C-l (^L, CTRL+L)
nnoremap <silent> <C-l> :nohl<CR><C-l>

luafile ~/.config/nvim/init.lua

" autocomplete framework (similar to emacs' company)
let g:deoplete#enable_at_startup = 1
packadd deoplete.nvim
" deoplete sources below
""""""""""""""""""""""""
packadd neco-vim " viml
packadd deoplete-jedi " python
""""""""""""""""""""""""

" Add matching pairs when opening parentheses
packadd auto-pairs

" check on the fly
packadd ale

" emacs' avy
packadd vim-easymotion

" julia
" deactivate any tab functionality because of issue
" either way, learn yourself some ⎡:help digraphs⎦
let g:latex_to_unicode_tab = 0                                                                                                                                                                                       
let g:latex_to_unicode_suggestions = 0                                                                                                                                                                               
let g:latex_to_unicode_eager = 0                                                                                                                                                                                     
let g:latex_to_unicode_file_types = ""                                                                                                                                                                               
let g:latex_to_unicode_cmd_mapping = []
packadd julia-vim
