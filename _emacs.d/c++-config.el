;;; c++-config.el --- C++ basic config (prolly will have separate local config)  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Felipe Lema

;; Author: Felipe Lema <felipelema@mortemale.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:



;;★★★★★★★★★★★★★★★★★★★★★ C++ indentation ★★★★★★★★★★★★★★★★★★★★★
(require 'cc-vars)
(defun my/set-cc-style-local ()
  "Set indentation on current buffer."
  (c-set-style "my-cc-style"))

;; check the comments (and next `add-hook`) for activating thiS
(defconst my-cc-style
  ;; "Do not indent a level when doing ⎡if(…)↲{⎦
  ;; see http://stackoverflow.com/a/2542054/3637404
  ;; see https://www.emacswiki.org/emacs/IndentingC#toc2
  '("linux"
    (c-offsets-alist . (
                        ;; Don't indent namespace
                        ;; http://stackoverflow.com/a/13825490/3637404
                        (innamespace . [0])
                        (brace-list-open . 0)
                        (substatement-open . 0)
                        (inline-open . 0)
                        ;; if you want to add an entry here,
                        ;; see https://stackoverflow.com/a/4267074/3637404
                        ))
    (c-basic-offset . 4)))
(c-add-style  "my-cc-style" my-cc-style)
(add-hook 'c++-mode-hook
          #'my/set-cc-style-local)

(require 'smartparens-config)
;; "{▌"→ RET → "{\n▌\n}"
(sp-local-pair 'c++-mode "{" nil :post-handlers '((my/create-newline-and-enter-sexp "RET")))
(defun my/create-newline-and-enter-sexp (&rest _ignored)
  "Open a new brace or bracket expression, with relevant newlines and indent."
  (newline)
  (indent-according-to-mode)
  (forward-line -1)
  (indent-according-to-mode))

(add-hook 'c++-mode-hook #'company-mode)

(provide 'c++-config)
;;; c++-config.el ends here
