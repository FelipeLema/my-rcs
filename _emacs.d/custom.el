(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "red3" "ForestGreen" "yellow3" "blue" "magenta3" "DeepSkyBlue" "gray50"])
 '(avy-keys
   (quote
    (97 111 101 117 104 116 110 115 59 113 106 107 109 119 118 122)))
 '(company-show-numbers t)
 '(display-line-numbers-type (quote visual))
 '(ediff-split-window-function (quote split-window-horizontally))
 '(eglot-connect-timeout 120)
 '(evil-digraphs-table-user
   (quote
    (((91 91)
      . 9121)
     ((93 93)
      . 9126)
     ((60 51)
      . 9829)
     ((44 46)
      . 8230)
     ((102 110)
      . 402)
     ((51 33)
      . 8802))) nil (evil-digraphs))
 '(frame-background-mode (quote dark))
 '(frecentf-also-store-dirname t)
 '(inhibit-startup-screen t)
 '(initial-scratch-message
   ";;Don't quote stuff. It's a living hell. Use `read-from-string'
(substitute-command-keys (concat \"\\\\\" \"{\" \"company-active-map\" \"}\"))

(with-current-buffer \"\"
  )

")
 '(jedi:environment-virtualenv (quote ("pyvenv" "--system-site-packages")))
 '(ls-lisp-verbosity nil)
 '(message-cite-reply-position (quote below))
 '(multi-term-dedicated-close-back-to-open-buffer-p t)
 '(multi-term-delete-windows t)
 '(multi-term-switch-after-close nil)
 '(term-bind-key-alist
   (quote
    (("C-c C-c" . term-interrupt-subjob)
     ("C-c C-e" . term-send-esc)
     ("C-p" . previous-line)
     ("C-n" . next-line)
     ("C-m" . term-send-return)
     ("C-y" . term-paste)
     ("M-f" . term-send-forward-word)
     ("M-b" . term-send-backward-word)
     ("M-o" . term-send-backspace)
     ("M-p" . term-send-up)
     ("M-n" . term-send-down)
     ("M-M" . term-send-forward-kill-word)
     ("M-N" . term-send-backward-kill-word)
     ("<C-backspace>" . term-send-backward-kill-word)
     ("C-s" . term-send-search-history)
     ("C-r" . term-send-reverse-search-history)
     ("M-d" . term-send-delete-word)
     ("M-," . term-send-raw)
     ("M-." . term-send-raw-meta))))
 '(tramp-verbose 2)
 '(vc-follow-symlinks t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(hl-sexp-background-colors (quote ("white" "mistyrose" "lightpink" "pink")))
 '(line-number-current-line ((t (:inherit line-number :foreground "yellow1"))))
 '(widget-field ((t (:background "cornsilk" :foreground "black")))))
