;;; elisp-config --- Elisp config -*- lexical-binding: t -*-
;;; Commentary:

;;; Code:

(add-hook 'emacs-lisp-mode-hook
	  (lambda ()
	    (highlight-regexp "%[sSodxXcefg]"
			      'hi-blue)))

(add-hook 'emacs-lisp-mode-hook #'company-mode)
(add-hook 'emacs-lisp-mode-hook #'flymake-mode)
;; show parenthesis match when in elisp
;; I know it has performance issues, but
;; right now I don't have big elisp files
(add-hook 'emacs-lisp-mode-hook #'show-paren-mode)

;; eldoc's cool AF
(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
(straight-use-package 'lisp-extra-font-lock)
(lisp-extra-font-lock-global-mode 1)


(require 'company)
(defun my/priorize-company-backend (σcompany-backend)
  "Priorize a specific ΣCOMPANY-BACKEND.
`add-to-list' doesn't cut it"
  (set (make-local-variable 'company-backends)
       (cons σcompany-backend
	     (delete σcompany-backend company-backends))))

;; this should be out-of-the-box. right now, it isn't
(add-hook
 'emacs-lisp-mode-hook
 (lambda ()
   (my/priorize-company-backend '(company-elisp :with company-capf))))

(when (boundp 'prettify-symbols-mode)
  (add-hook 'emacs-lisp-mode-hook #'prettify-symbols-mode)
  (add-hook 'emacs-lisp-mode-hook
	    (lambda ()
	      (push '("format" . ?φ) prettify-symbols-alist))))

(require 'evil)
(evil-define-key 'normal emacs-lisp-mode-map
  (kbd "[{") #'sp-backward-up-sexp)
(evil-define-key 'normal emacs-lisp-mode-map
  (kbd "]}") #'sp-down-sexp)

(evil-define-key 'normal emacs-lisp-mode-map
  (kbd "M-.") #'find-function-at-point)

(defun my/eval-region-else-last-sexp (&optional begin end)
  "Eval selected region ⎡BEGIN→END⎦ (or last sexp if there is no selection)."
  (interactive
   (when (mark)
     (list (region-beginning) (region-end))))
  (if (evil-visual-state-p)
      ;;(my/eval-string-for-side-effects "(daemonp)")
      (eval (car (read-from-string (buffer-substring begin end))))
    ;; else
    (call-interactively 'eval-last-sexp)))

;; highlight max column (for limiting line width)
;; see https://www.emacswiki.org/emacs/EightyColumnRule#toc5
(require 'whitespace)
(add-hook 'emacs-lisp-mode-hook
	  (lambda ()
	    (setq-local whitespace-style '(face lines-tail trailing))
	    (whitespace-mode t)))




;; company for `*scratch*' buffer
(require 'dash)
(require 's)
(defvar my/with-current-buffer-re  "([ ]*with-current-buffer[ ]+\"\\(.*\\)")
(defun company-elisp-buffers-get-completions (prefix)
  "Get buffers when PREFIX is `(with-current-buffer \"…)'."
  (pcase prefix
    ((pred stringp)
     (let* (
            ;; assuming currentLine is up til cursor (see `company-grab-line')
            (bufferNames (--map
                          (buffer-name it)
                          (buffer-list)))
            (buffersStartingWith
             (--filter
              (string-prefix-p prefix it)
              bufferNames))
            )
       buffersStartingWith))
    ('nil nil)
    (_ (error "Unexpected type: %s" prefix))))
(defun company-elisp-buffers (command &optional arg &rest ignored)
  "Company interface for backend (handles COMMAND, ARG, IGNORED)."
  (cl-case command
    (interactive (company-begin-backend 'company-elisp-buffers-backend))
    (prefix
     ;;                ↓ grabs until cursor
     (let ((maybeLine (company-grab-line my/with-current-buffer-re)))
       (if (stringp maybeLine)
           (cadr
            (s-match my/with-current-buffer-re
                     maybeLine)))))
    (candidates (company-elisp-buffers-get-completions arg))))
(add-hook
 'lisp-interaction-mode-hook
 (lambda ()
   (my/priorize-company-backend 'company-elisp-buffers)))

;; https://oremacs.com/2017/12/27/company-numbers/
;; don't forget to `customize-variable' `company-show-numbers'
(--each (number-sequence 0 9)
  (define-key company-active-map (format "%d" it) 'ora-company-number))

(defun ora-company-number ()
  "Forward to `company-complete-number'.

Unless the number is potentially part of the candidate or
the number is outside the number of candidates.
In that case, insert the number."
  (interactive)
  (let* ((k (this-command-keys))
         (k-is-number (s-matches? (rx (+ digit)) k))
         (k-as-number (string-to-number k))
         (re (concat "^" company-prefix k)))
    (if (or
         ;; ↓ is a number and number is outside the number of candidates
	 (and k-is-number
              (> k-as-number (length company-candidates)))
         (--find (string-match re it) company-candidates))
        (self-insert-command 1)
      ;; else
      (company-complete-number (if (s-equals? k "0")
                                   10
                                 k-as-number)))))

;; https://github.com/syl20bnr/spacemacs/issues/10956#issuecomment-401370399
(defun config//term-normal-state ()
  "Enable `term-line-mode' when in normal state in `term-mode' buffer and make the buffer read only."
  (term-line-mode)
  (read-only-mode 1))
(defun config//term-insert-state ()
  "Enable `term-char-mode' when in insert state in a `term-mode' buffer."
  (when (get-buffer-process (current-buffer))
    (read-only-mode -1)
    (term-char-mode)
    ))
(defun config//term-evil-bindings ()
  "Enable term support for vim and hybrid editing styles."
  (add-hook 'evil-hybrid-state-entry-hook 'config//term-insert-state nil t)
  (add-hook 'evil-insert-state-entry-hook 'config//term-insert-state nil t)
  (add-hook 'evil-hybrid-state-exit-hook 'config//term-normal-state nil t)
  (add-hook 'evil-insert-state-exit-hook 'config//term-normal-state nil t))
(setq term-char-mode-point-at-process-mark t)
(add-hook 'term-mode-hook 'config//term-evil-bindings)

(straight-use-package 'elisp-def)

;;; colored/rainbowed outline
(straight-use-package 'outshine)
(add-hook 'emacs-lisp-mode-hook 'outline-minor-mode)
(add-hook 'outline-minor-mode-hook #'outshine-mode)


(provide 'elisp-config)
;;; elisp-config.el ends here
