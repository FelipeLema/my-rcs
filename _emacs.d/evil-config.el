;;; evil-config.el --- Evil configuration            -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Felipe Lema

;; Author: Felipe Lema <felipelema@mortemale.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(require 'dash)
(eval-when-compile
  (require 'cl))
(require 'straight)
(straight-use-package 'evil)
(evil-mode 1)

(straight-use-package 'evil-matchit)
(straight-use-package 'evil-smartparens)

(straight-use-package 'evil-magit)
(require 'evil-magit)

;; * and # searches for word and not sub-word
;; see http://www.reddit.com/r/emacs/comments/2eqn9f/a_peek_at_emacs_244_superwordmode/ck2pidr
(setq-default evil-symbol-word-search t)
;; ⎡C-w ←⎦,⎡C-w ↑⎦,… shortcuts (with arrows)
(define-key evil-window-map (kbd "<up>") #'evil-window-up)
(define-key evil-window-map (kbd "<down>") #'evil-window-down)
(define-key evil-window-map (kbd "<left>") #'evil-window-left)
(define-key evil-window-map (kbd "<right>") #'evil-window-right)

;; deactivate evil mode for term (good call, neovim)
;; you can also do ⎡C-z⎦
(evil-set-initial-state 'term-mode 'emacs)
;; calendar (when using dates in org with ⎡C-c <⎦ & ⎡C-c >⎦)
(evil-set-initial-state 'calendar-mode 'emacs)
(evil-set-initial-state 'inferior-python-mode 'normal)
(evil-set-initial-state 'helpful-mode 'motion)
(evil-set-initial-state 'magit-status-mode 'emacs)
(evil-set-initial-state 'mu4e-compose-mode 'insert)

;; evil-snipe's cool ΑF
;; not as cool as easymotion, tho
;; Imma let you finish, but avy's got the best motion of all time
(straight-use-package 'avy)
(straight-use-package 'evil-avy)
(evil-avy-mode 1)
;; does not work → (define-key evil-visual-state-map (kbd "s") #'avy-goto-char-in-window)
(defun avy-goto-char-in-window (char &optional arg)
  (interactive (list (read-char "char: " t)
		     current-prefix-arg))
  (avy-goto-char char t))
(evil-define-motion evil-goto-char-in-window (count char)
  "Use avy to move forward to char in line."
  :jump t
  :type inclusive
  (interactive "<c><C>")
  (if (null count)
      (avy-goto-char-in-window char)
    (error "No counter operation for avy-goto-char-in-window")))
(evil-define-key 'visual evil-avy-mode-map
  "s" 'evil-goto-char-in-window)

;;(straight-use-package 'evil-space)
;;(evil-space-mode 1) ;; repeat motion with space

;; https://github.com/noctuid/evil-guide#mode-specific-keybindings
(evil-define-key 'normal org-mode-map
  (kbd "TAB") 'org-cycle)

(evil-define-key 'motion help-mode-map
  (kbd "TAB") #'forward-button)

;; reformatted `evil-ex-show-digraphs'
(defun my/select-digraph ()
  "Narrow selection of digraph.
Based off helm-unicode"
  (interactive)
  (-uniq
   (let* ((all-digraphs (append evil-digraphs-table
				evil-digraphs-table-user))
          (formated-digraphs (-map (-lambda ((keys . char))
				     `((char . ,char)
				       (keys . ,keys)
				       (name . ,(get-char-code-property char 'name))))
                                   all-digraphs))
          ;; not all digraphs have name ¯\_(ツ)_/¯
          (named-digraphs (--filter (alist-get 'name it)
                                    formated-digraphs))
          (digraphs-string-list (sort (--map
                                       (let-alist it
                                         (format "%c\t%c%c\t%s"
					    .char
					    (car .keys)
					    (cadr .keys)
					    .name))
                                       named-digraphs)
                                      #'string-lessp)))
     (save-excursion
       (goto-char (+ 1 (point))) ;; `insert' inserts before cursor
       (insert (-first-item
                (s-split "\t"
                         (ivy-read "Digraph:" digraphs-string-list))))))))


;; terminal movement changed
;; I always try to use updated emacs, so don't care about the situation in
;; previous versions
(defun my/writing-in-term ()
  (when (get-buffer-process (current-buffer))
    (setq term-char-mode-point-at-process-mark t
	  term-char-mode-buffer-read-only nil)))
(defun my/not-writing-in-term ()
  (setq term-char-mode-point-at-process-mark nil
	term-char-mode-buffer-read-only t))
(defun my/config-term-for-evil ()
  (when (version<= "26.1" emacs-version)
    (add-hook 'evil-emacs-state-entry-hook 'my/writing-in-term nil t)
    (add-hook 'evil-emacs-state-exit-hook 'my/not-writing-in-term nil t)))
(add-hook 'term-mode-hook #'my/config-term-for-evil)


;;; I keep pressing M-. instead of M->, which has horrible repercussions
(evil-define-key nil 'global (kbd "M-.") nil)
;;; `find-file-at-point' sυcks aσσ
(defun my/decay-to-find-file ()
  "Forward INITIAL-INPUT to `counsel-find-file' according to major mode."
  (interactive)
  (pcase major-mode
    ;; probably python or c++ go here
    (`python-mode (call-interactively 'find-file-at-point))
    ;; not elisp, use `elisp-def' instead
    (_ (if-let* ((maybe-file (thing-at-point 'filename))
                 (dir (cond
                       ((file-directory-p maybe-file) maybe-file)
                       ((file-exists-p maybe-file)
			(or (file-name-directory maybe-file)
                            "./")) ;; assume $PWD when file has no directory
                       (t nil))))
           (let ((actual-file (cond
                               ((file-directory-p maybe-file) nil)
                               (t (file-name-nondirectory maybe-file))))
                 (default-directory ;; ←↓ try not to use `file-truename'
                   (file-name-as-directory
                    (if-let* ((maybe-relative-dir (concat
                                                   (file-name-as-directory default-directory)
                                                   (s-replace-regexp (rx line-start (+ "./"))
                                                                     ""
                                                                     dir)))
                              (dir-is-relative (file-exists-p maybe-relative-dir)))
			;; relative directory
			maybe-relative-dir
                      ;; absolute directory
		      dir))))
             (counsel-find-file actual-file))
         ;; else-let*
	 (call-interactively 'counsel-find-file)))))

(evil-define-key 'normal 'global (kbd "g f") #'my/decay-to-find-file)


(provide 'evil-config)
;;; evil-config.el ends here
