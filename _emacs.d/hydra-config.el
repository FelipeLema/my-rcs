;;; hydra-config.el --- vim prefix config using hydra  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Felipe Lema

;; Author: Felipe Lema <felipelema@mortemale.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'straight)
(straight-use-package 'hydra)
(require 'counsel) ;; counsel-evil-registers
(require 'evil)
(require 'dash)

(defun my/jump-to-related ()
  "Goto definition/usages depending on main mode."
  (interactive)
  (call-interactively
   (pcase major-mode
     (`emacs-lisp-mode 'find-function-at-point)
     (`c++-mode 'ggtags-find-tag-from-here)
     (_ (error "Don't know what function I should use with mode %s" major-mode))
     )))
(defun my/del-and-switch-buffer ()
  "Delete buffer, but keep open and select \"other\" window."
  (interactive)
  (let ((_delete_me   (current-buffer)))
    (previous-buffer)
    (evil-delete-buffer _delete_me))
  )

(defhydra my/buffer-hydra (:exit t)
  ("b" bookmark-jump "jump to bookmark")
  ("d" evil-delete-buffer "delete buffer")
  ("f" frecentf-pick-file "delete buffer")
  ("o" my/del-and-switch-buffer "del&switch-other-b")
  ("s" switch-to-buffer "switch-to-buffer"))


(defhydra my/window-hydra (:exit t)
  "window"
  ("c" evil-window-delete "close")
  ("h" evil-window-left "left")
  ("j" evil-window-down "down")
  ("k" evil-window-up "up")
  ("l" evil-window-right "right")
  ("o" delete-other-windows "close other")
  ("s" evil-window-split "h-split")
  ("v" evil-window-vsplit "v-split")
  ("=" balance-windows "= size all")
  )
(defhydra my/customize-hydra (:exit t)
  ("f" customize-face "face")
  ("v" customize-variable "variable"))

(defhydra my/help-hydra (:exit t)
  "help"
  ("v" describe-variable "variable")
  ("f" describe-function "function")
  ("k" describe-key "key")
  ("m" info-emacs-emacs "emacs")
  ("e" info-elisp "elisp")
  )

(defhydra my/lang-hydra (:exit t)
  "lang"
  ("." my/jump-to-related "tag find")
  ("0" last-error "last error")
  ("1" first-error "first error")
  ("i" imenu "imenu")
  ("n" flymake-goto-next-error "next error")
  ("p" flymake-goto-prev-error "previous error"))

(defhydra my/elisp-hydra (:exit t)
  "elisp"
  ("a" auto-insert "insert header")
  ("b" eval-buffer "ε buffer")
  ("e" my/eval-region-else-last-sexp "ε last sexp")
  )

;; Main hydra
(defhydra my/leader-evil+hydra (:exit t)
  "Hydra!"
  ("/" swiper)
  ("c" my/customize-hydra/body)
  ("e" my/elisp-hydra/body)
  ("f" counsel-find-file)
  ("g" (counsel-rg (thing-at-point 'symbol)))
  ("h" my/help-hydra/body)
  ("l" my/lang-hydra/body)
  ("q" save-buffers-kill-terminal "C-x C-c")
  ("r"
   (let ((n-lines (-reduce-from
		   (lambda (memo item)
		     (+ memo 1 (if (cdr item)
				   (length (s-matched-positions-all "\n" (cdr item)))
				 0)))
		   0
		   (evil-register-list)))
	 (key 'counsel-evil-registers))
     (setq ivy-height-alist
	   (append `((,key . ,n-lines))
		   (assq-delete-all key ivy-height-alist)))
     (call-interactively 'counsel-evil-registers))
   ":reg")
  ("s" save-buffer)
  ("t" multi-term)
  ("u" my/buffer-hydra/body)
  ("v" avy-goto-char)
  ("w" my/window-hydra/body)
  ("z" evil-save-modified-and-close ":wq")
  )

(defun my/call-hydra (hydra)
  "Call (main) HYDRA WITHOUT getting the keystrokes to (evil-)repeat."
  (let* ((hydra/bodyStr (format "%s/body" (symbol-name hydra)))
	 (callHydraStr (format "(call-interactively #'%s)" hydra/bodyStr))
	 (callHydra (car (read-from-string callHydraStr))))
    (evil-without-repeat
      (let ((hydra-is-helpful nil))
	(eval callHydra)))))

(defun my/call-main-hydra ()
  "Command wrapper for `my/call-hydra' on main hydra."
  (interactive)
  (my/call-hydra 'my/leader-evil+hydra)
  )


;; https://github.com/noctuid/evil-guide#leader-key
(-map
 (-lambda ((map key))
   (eval `(define-key ,map ,key 'my/call-main-hydra))
   )
 (-flatten-n 1 (-table 'list
		       ;; maps
		       '(evil-normal-state-map
			 evil-motion-state-map
			 evil-visual-state-map)
		       ;; keys
		       '("\\" " "))))
(add-hook 'dired-mode-hook (lambda ()
			     (evil-local-set-key 'normal
						 (kbd " ")
						 'my/leader-evil+hydra/body)))



(provide 'hydra-config)
;;; hydra-config.el ends here
