;;; init.el --- Start-up config                      -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Felipe Lema

;; Author: Felipe Lema <felipelema@mortemale.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


;; (customize …) at the end
;; Garbage collect lazily to speed up
;; https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/
;; http://emacs.stackexchange.com/a/16595/10785
(let ((gc-cons-threshold (* 100 1024 1024))
      (file-name-handler-alist nil))

  ;; https://github.com/raxod502/straight.el
  (let ((bootstrap-file
	 (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	(bootstrap-version 4))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
	  (url-retrieve-synchronously
	   "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	   'silent 'inhibit-cookies)
	(goto-char (point-max))
	(eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)
    (ignore bootstrap-version))

  (let ((melpapackages '(company
			 company-jedi
			 counsel
			 jedi
			 magit
			 persist
			 rainbow-delimiters
			 s
			 smartparens
			 yasnippet))
	(recipes `((electric-align
		    :type git :host github :repo "zk-phi/electric-align"))))
    (dolist (recipe (append melpapackages recipes))
      (straight-use-package recipe)))
  (straight-use-package 'doom-modeline)
  (require 'doom-modeline)
  (doom-modeline-init)
  (straight-use-package '(multi-term
			  :type git
			  :repo "https://git.launchpad.net/multi-term.el"))

  ;; this ↓ is not documented (allows not-in-middle-of-word completion)
  ;;(setq-local company-auto-complete t)
  ;;(setq-local company-auto-complete-chars '(?/ ) ))

  (load-file "~/.emacs.d/evil-config.el")
  (load-file "~/.emacs.d/hydra-config.el")



  ;; missing file name associations
  (dolist (e '(
	       ;; https://www.emacswiki.org/emacs/AutoModeAlist
	       ("/\\.z.*\\'" . sh-mode)
	       ))
    (add-to-list 'auto-mode-alist e))

  ;; line truncation / line wrap ↲ ↩
  (set-display-table-slot standard-display-table 'wrap ?↩)
  (set-display-table-slot standard-display-table 'truncation ?⍈)

  ;; Non-ascii characters for whitespace-mode (whitespace-newline-mode)
  ;; see http://ergoemacs.org/emacs/whitespace-mode.html
  (setq whitespace-display-mappings
	;; all numbers are Unicode codepoint in decimal. try (insert-char 182 ) to see it
	'((space-mark 32 [183] [46]) ; 32 SPACE, 183 MIDDLE DOT 「·」, 46 FULL STOP 「.」
	  (newline-mark 10 [182 10]) ; 10 LINE FEED
	  (tab-mark 9 [9655 9] [92 9]) ; 9 TAB, 9655 WHITE RIGHT-POINTING TRIANGLE 「▷」
	  ))

  ;; python3 as default
  ;; can run using M-x run-python
  ;; see python.el
  (setq python-shell-interpreter (if (executable-find "pypy3")
				     "pypy3"
				   "python3"))
  ;; from the docs (for company+python)
  ;; see http://tkf.github.io/emacs-jedi/latest/#how-to-use-python-3-or-any-other-specific-version-of-python
  ;; you'll have to do this yourself (I tried with elisp jedi cmds but fuck it)
  ;; mkdir -p ~/.emacs.d/.python-environments; cd $_; pyvenv --system-site-packages jedi-python3; source $_/bin/activate; pip install jedi
  (setq jedi:environment-root "jedi-python3")  ; or any other name you like

  ;; Best option so far for 2-space indentation for python
  ;; see http://stackoverflow.com/a/4251387/3637404
  ;; Python Hook
  (add-hook 'python-mode-hook
	    (lambda ()
	      (setq indent-tabs-mode nil
		    tab-width 2)))
  (require 'yasnippet)
  (if (daemonp)
      (yas-reload-all))
  (add-hook 'python-mode-hook #'yas-minor-mode)
  ;; for python docs
  (add-hook 'rst-mode-hook 'evil-motion-state)
  ;; fold to …
  ;; C-h v hs-set-up-overlay
  ;; AFTER you M-x hs-minor-mode
  (defun my-display-fold (ov)
    (when (eq 'code (overlay-get ov 'hs))
      (overlay-put ov 'display "…")))
  (setq hs-set-up-overlay 'my-display-fold)

  ;; Turn off menu in console
  ;; access it with F10
  (unless window-system
    (menu-bar-mode 0))

  ;; Backup files (as in "main.cpp~")
  ;; to a separate folder
  (setq backup-directory-alist `(("." . "~/.saves")))

  (add-to-list 'evil-emacs-state-modes 'shell-mode-hook)

  ;; looking for command palette (see Jupyter 4.1 release)
  ;; http://stackoverflow.com/a/32844122/3637404

  (global-set-key (kbd "M-x") #'counsel-M-x)
  (require 'persist)
  (persist-defvar counsel-M-x-history nil
		  "History for `counsel-M-x'.")
  (evil-global-set-key 'normal (kbd "DEL") #'counsel-M-x)  ;; use backspace⌫ for something useful (I always use ⎡h⎦ or ⎡←⎦ instead)
  ;; `find-file-at-point' sucks ass (maybe counsel↔evil bug?)
  (evil-global-set-key 'normal (kbd "g f") #'counsel-find-file)


  (global-set-key (kbd "<f12>") #'woman)


  (add-hook 'python-mode-hook #'company-mode)
  (add-hook 'python-mode-hook (lambda () (add-to-list 'company-backends 'company-jedi)))
  (add-hook 'python-mode-hook #'jedi:setup)
  (add-hook 'sh-set-shell-hook #'company-mode)


  ;; see http://emacs.stackexchange.com/a/10838/10785
  (setq company-dabbrev-downcase nil)

  ;; writing << produces some "<< EOF" stuff
  ;; see http://emacs.stackexchange.com/questions/5336/why-does-typing-instead-produce-eof-n-when-in-shell-script-mode
  (add-hook 'sh-mode-hook (lambda () (sh-electric-here-document-mode -1)))


  ;; zsh has a weird command prompt... this solves it
  ;; ¯\_(ツ)_/¯
  ;;(add-to-list 'load-path "~/my-rcs")
  ;;(require 'popwin-term)

  ;; don't use multi-compile, just don't


  ;; Fallback font for characters
  ;; see http://stackoverflow.com/a/6084198/3637404 🖒👍
  ;; only available in gui
  (if (display-graphic-p)
      (set-fontset-font "fontset-default" '() "Symbola"))

  ;; Forget it, learn vim digraphs ⎡:he digraphs⎦
  ;; ⎡special totally-frequently-used parenthesis⎦
  ;; http://emacs.stackexchange.com/a/7294/10785
  ;; not the same as \lceil and \rfloor, but ⎡Asian bracket⎦
  ;; see also http://xahlee.info/comp/unicode_matching_brackets.html
  ;;;(define-key 'iso-transl-ctl-x-8-map "[" [?⎡])
  ;;;(define-key 'iso-transl-ctl-x-8-map "]" [?⎦])
  ;; again, thanks https://gist.github.com/areina/3879626
  ;; non-versioned mail info, though
  (let ((mail-info  "~/.emacs.d/mail-info.el"))
    (and (file-readable-p mail-info)
	 (load-file mail-info)))

  ;; attach files w/mu4e using dired
  ;; see https://www.djcbsoftware.nl/code/mu/mu4e/Dired.html#Dired
  ;; while composing, call Dired =C-x d=,
  ;; mark w/ =m= and attach w/ =C-c RET C-a=
  (require 'gnus-dired)
  ;;; make the `gnus-dired-mail-buffers' function also work on
  ;;; message-mode derived modes, such as mu4e-compose-mode
  (defun gnus-dired-mail-buffers ()
    "Return a list of active message buffers."
    (let (buffers)
      (save-current-buffer
	(dolist (buffer (buffer-list t))
	  (set-buffer buffer)
	  (when (and (derived-mode-p 'message-mode)
		     (null message-sent-message-via))
	    (push (buffer-name buffer) buffers))))
      (nreverse buffers)))
  (setq gnus-dired-mail-mode 'mu4e-user-agent)
  (add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)


  ;; Autocomplete address-related fields
  (add-hook 'mu4e-compose-mode-hook
	    (lambda ()
	      (company-mode t)))


  ;; http://stackoverflow.com/a/20233611
  (defun my/default-terminal-background ()
    (unless (display-graphic-p (selected-frame))
      (set-face-background 'default "unspecified-bg" (selected-frame))))

  (add-hook 'window-setup-hook 'my/default-terminal-background)



  ;; week starts on monday
  (setq calendar-week-start-day 1)

  (defun my/get-password-if-prompted (process string)
    "Filter the PROCESS output (STRING) and prompt password if asked."
    ;;(message "%s" string)
    (if (s-match "Please enter the passphrase to unlock the OpenPGP secret key:"
		 string)
	(save-window-excursion
	  (display-buffer (process-buffer process))
	  (let ((password (read-passwd "Password for gpg key: ")))
	    (process-send-string process (format "%s\n" password))
	    )))
    ;; else, just let it through
    (process-send-string process string))

  (defun my/update-mail (call-mu4e-when-done)
    "Update mail and maybe CALL-MU4E-WHEN-DONE."
    (let* ((buf (term "zsh"))
	   (buf-proc (get-buffer-process buf)))
      (set-process-sentinel
       buf-proc
       ;; multi-term-handle-close
       (lambda (proc change)
	 (when (string-match "\\(finished\\|exited\\)" change)
	   (kill-buffer (process-buffer proc))
	   (when call-mu4e-when-done
	     (mu4e)))))
      ;;(set-process-filter buf-proc 'my/get-password-if-prompted)
      (switch-to-buffer buf
			(term-send-raw-string "offlineimap && mu index && exit\n"))))
  (defun my/open-mail ()
    "Update and open mail"
    (interactive)
    (my/update-mail t))
  ;; update mail when exiting mu4e
  (defun my/quit-m4e-and-update ()
    (interactive)
    (mu4e-quit)
    (my/update-mail nil))
  (defun my/bind-q-to-mu4e-and-update ()
    (local-set-key (kbd "q") #'my/quit-m4e-and-update))
  (add-hook 'mu4e-main-mode-hook #'my/bind-q-to-mu4e-and-update)
  ;; open html in browser
  (when (boundp 'mu4e-view-actions)
    (add-to-list 'mu4e-view-actions
		 '("ViewInBrowser" . mu4e-action-view-in-browser)
		 ))

  (add-hook 'dired-after-readin-hook #'dired-hide-details-mode)

  ;; highlight symbol under cursor

  ;; I use this a lot
  (global-set-key (kbd "<f5>") #'multi-term)
  (defun term-send-search-history ()
    "Search history reverse."
    (interactive)
    (term-send-raw-string "\C-s"))

  ;; language-specific
  (load "~/.emacs.d/elisp-config.el")
  (load "~/.emacs.d/c++-config.el")
  (load "~/.emacs.d/julia-config.el")

  ;; yuck
  (defadvice auto-complete-mode (around disable-auto-complete-for-python)
    (unless (eq major-mode 'python-mode) ad-do-it))

  (ad-activate 'auto-complete-mode)

  (require 'smartparens-config)
  (smartparens-global-mode)

  (require 'smartparens)
  (sp-pair "⎡" "⎦")
  ;; see http://emacs.stackexchange.com/a/29293/10785
  ;; see https://stackoverflow.com/a/31870872/3637404 for   ↓
  (defun sp/prompt-delimiter-char (delimiter-char &optional _)
    (interactive (list (read-char "delimiter char: " t)
		       current-prefix-arg))
    delimiter-char)
  (evil-define-operator sp/wrap-with-parens (begin end)
    (let ((delimiter-char (call-interactively #'sp/prompt-delimiter-char)))
      (evil-visual-expand-region)
      (sp-wrap-with-pair (format "%c" delimiter-char))))
  (evil-define-key 'visual global-map (kbd "(") 'sp/wrap-with-parens)

  ;; "fix" filename/filepath completion in sh mode [zsh]
  (require 'comint)
  (add-hook
   'sh-mode-hook
   (lambda ()
     (setq-local comint-file-name-chars
		 (replace-regexp-in-string "[#={}]" "" comint-file-name-chars))))
  ;; indent on the fly
  (straight-use-package 'aggressive-indent)
  (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
  (add-hook 'lisp-interaction-mode-hook #'aggressive-indent-mode)

  (require 'electric-align)
  (add-hook 'prog-mode-hook 'electric-align-mode)

  ;; colored matching parentheses ♥
  (require 'outline) ;; has the colors for rainbow-delimiters in exotica theme
  (require 'rainbow-delimiters)
  (add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode)

  (ivy-mode t)

  ;; block cursor
  (unless (display-graphic-p)
    (defvar evil-motion-state-cursor)
    (defvar evil-visual-state-cursor)
    (defvar evil-normal-state-cursor)
    (defvar evil-insert-state-cursor)
    (defvar evil-emacs-state-cursor)
    (setq evil-motion-state-cursor 'box)  ; █
    (setq evil-visual-state-cursor 'hbar) ; _
    (setq evil-normal-state-cursor 'box)  ; █
    (setq evil-insert-state-cursor 'bar)  ; ⎸
    (setq evil-emacs-state-cursor  'box)  ; █
    (straight-use-package `(evil-terminal-cursor-changer
                            :type git :host github
                            :repo "amosbird/evil-terminal-cursor-changer"))
    (require 'evil-terminal-cursor-changer)
    (evil-terminal-cursor-changer-activate)) ; or (etcc-on)

  (straight-use-package 'company-statistics)
  (add-hook 'after-init-hook 'company-statistics-mode)

  ;; no toolbars
  (when (display-graphic-p)
    (menu-bar-mode -1)
    (toggle-scroll-bar -1)
    (tool-bar-mode -1)

    ;; represent evil state in cursor color
    (setq evil-normal-state-cursor '(box "#cccccc") ;; "gray?"
	  evil-emacs-state-cursor '(box "HotPink")
	  evil-insert-state-cursor '((bar . 2) "#cdcd00"))

    (blink-cursor-mode 0))

  (straight-use-package '(frecentf
			  :type git
			  :repo "https://git.launchpad.net/frecentf.el"))
  (frecentf-mode 1)

  (straight-use-package 'info-colors)
  (add-hook 'Info-selection-hook 'info-colors-fontify-node)

  (straight-use-package '(noctilux-theme
			  :type git
			  :repo "https://git.launchpad.net/noctilux-theme"))

  (let ((local_settings  "~/.emacs.d/local_settings.el"))
    (if (file-exists-p local_settings)
	(load local_settings)))
  (setq custom-file (concat
		     (file-name-as-directory user-emacs-directory)
		     "custom.el"))
  (load custom-file)
  ;; load theme must go after loading custom file
  (load-theme 'noctilux t))


(provide 'init)
;;; init.el ends here
