;;; julia-config.el --- julia language config        -*- lexical-binding: t; -*-

;; Copyright © 2019

;; Author:  <felipelema@mortemale.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Install LanguageServer using `Pkg.dev("LanguageServer"); Pkg.add("SymbolServer")'
;; Install PackageCompiler using `Pkg.dev("PackageCompiler")'
;; Compile like `using PackageCompiler; compile_package("LanguageServer")'

;;; Code:

(require 'straight)
(straight-use-package 'julia-mode)
(straight-use-package 'eglot)
(require 'eglot)
(require 'julia-mode)
(require 's)

(add-to-list 'eglot-server-programs
	     ;; inspired partly in
	     ;; https://github.com/non-Jedi/lsp-julia/blob/master/lsp-julia.el
	     (let* ((working-dir (format "\"%s\""
	                            (file-truename (or default-directory
	                                               (getenv (if (daemonp)
								   "HOME"
								 "PWD"))))))
	            (depot-path (format "\"%s\""
	                           (if-let ((julia-depot-path
					     (getenv "JULIA_DEPOT_PATH")))
	                               (file-truename julia-depot-path)
	                             "")))
	            (server-args (s-join ", "`("stdin" "stdout" "false"
	                                       ,working-dir ,depot-path "Dict()")))) 
	       `(julia-mode . ("julia" "--startup-file=no" "--history-file=no"
			       "-e"
			       ,(s-join ";"
					`("using LanguageServer, Sockets, SymbolServer"
					  ,(s-lex-format
					    "server = LanguageServer.LanguageServerInstance(${server-args})")
					  "server.runlinter = true"
					  "run(server)"))))))
(add-hook 'julia-mode-hook #'eglot-ensure)



(provide 'julia-config)
;;; julia-config.el ends here
