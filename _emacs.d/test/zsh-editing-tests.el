;;; zsh-mode-tests.el --- zsh-mode unit tests        -*- lexical-binding: t; -*-

;; Copyright © 2018

;; Author:  <felipelema@mortemale.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'ert)
(require 'zsh-mode)

(defconst zsh-mode-tests--repo-root
  (locate-dominating-file
   (or
    (if-let ((buffer-filepath (buffer-file-name)))
	(file-name-directory buffer-filepath)) ;; running within emacs
    "./" ;; running as ⎡make test⎦
    )

   ".git"))
(defconst zsh-mode-tests--test-directory
  (concat (file-name-as-directory zsh-mode-tests--repo-root)
	  "test"))
(defconst zsh-mode-tests--sample-filepath
  (concat (file-name-as-directory zsh-mode-tests--test-directory)
	  "sample_script"))

(defmacro zsh-mode-tests--with-sample-script (&rest body)
  "Use sample_script as current buffer and eval BODY."
  `(with-temp-buffer
     (insert-file-contents zsh-mode-tests--sample-filepath)
     (let ((inhibit-message t))
       (sh-mode)
       (sh-set-shell "zsh"))
     ,@body))

(ert-deftest zsh-mode-tests--in-zsh ()
  "Is/isn't in a \"zsh\" editing mode "
  (zsh-mode-tests--with-sample-script
   (should (zsh--in-zsh)))
  (with-temp-buffer
    (let ((inhibit-message t))
      (sh-mode)
      (sh-set-shell "bash"))
    (should (not (zsh--in-zsh))))
  (with-temp-buffer
    (let ((inhibit-message t))
      (python-mode))
    (should (not (zsh--in-zsh)))))

(ert-deftest zsh-mode-tests--variables ()
  (zsh-mode-tests--with-sample-script
   (forward-line 6)
   (should (-all?
	    (-lambda ((got . expected))
	      (s-equals? got expected))
	    (-zip (zsh--get-local-variables) '("local_variable"))))
   (should (-all?
	    (-lambda ((got . expected))
	      (s-equals? got expected))
	    (-zip (zsh--get-set-variables) '("global_variable"))))
   (forward-line 10)
   (should (-all?
	    (-lambda ((got . expected))
	      (s-equals? got expected))
	    (-zip (zsh--get-loop-variables) '("i"))))))

(ert-deftest zsh-mode-tests--in-middle-of-variable-name ()
  (zsh-mode-tests--with-sample-script
   (goto-char (point-min))
   (search-forward "${PATH}")
   (backward-char 3)
   (should (zsh--in-middle-of-variable-name))
   (search-forward "$some_var")
   (backward-char 3)
   (search-forward "$ # ")
   (backward-char 3)
   (should (zsh--in-middle-of-variable-name))))

(ert-deftest zsh-mode-tests--completion-at-point ()
  (zsh-mode-tests--with-sample-script
   (goto-char (point-min))
   (search-forward "${PATH}")
   (backward-char 3)
   (let ((candidates (-third-item (zsh-completion-at-point))))
     (should (-contains? candidates "PATH")))))

(provide 'zsh-mode-tests)
;;; zsh-mode-tests.el ends here
