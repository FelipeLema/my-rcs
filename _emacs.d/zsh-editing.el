;;; zsh-mode.el --- Enhance Emacs' `sh-mode` specifically for editing zsh scripts.  -*- lexical-binding: t; -*-

;; Copyright © 2018  Felipe Lema

;; Author: Felipe Lema <felipelema@mortemale.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;; Thought for zsh, may be useful for bash, but don't asume it is
;;

;;; Code:
(require 'cl-lib)
(require 'dash)
(require 's)
(require 'sh-script)

(defconst zsh--variable-name-re
  '(seq (+ (any alpha ?_)))
  "Regex to match a variable name.

Use as ⎡(rx … (eval zsh--variable-name-re) …)⎦")

(defun zsh--in-zsh ()
  "Tell if current buffer is a zsh buffer.

https://stackoverflow.com/a/1408990/3637404"
  (and (eq major-mode 'sh-mode) (-contains? '(zsh zsh\ -f) sh-shell)))

(defun zsh--get-environment-variables ()
  "Fetch all environment variables as a list."
  (--map
   ;; s-match returns a list of matches (we just want the first)
   (car (s-match (rx (eval zsh--variable-name-re)) it))
   process-environment))

(defun zsh--enumerate-lines (lines-str &optional offset)
  "Add line number as text property to LINES-STR starting count at OFFSET."
  (let* ((line-offset (or offset 0))
	 (lines-as-ranges (s-matched-positions-all
			   (rx line-start (* not-newline) line-end)
			   lines-str))
	 (propertized-each-line
	  (--map-indexed
	   (let ((line-begin (car it))
		 (line-end (cdr it)))
	     (propertize (substring-no-properties lines-str
						  line-begin
						  line-end)
			 'line-number it-index))
	   lines-as-ranges)))
    (s-join "\n" propertized-each-line)))

(defun zsh--get-local-variables (&optional open-brace-count)
  "Get ⎡local …⎦ variable names in current buffer.
Use OPEN-BRACE-COUNT for excluding foreign blocks.

Line number is stored as `line-number' property."
  (let ((open-brace-count (or open-brace-count 0))
        (curly-brace-re (rx line-start
                            (* space)
                            (any ?{ ?}) ))
        (current-point (point)))
    (save-excursion
      ;; go backwards looking for opening brace (or discard other inner blocks
      ;; along the way)
      (when (re-search-backward curly-brace-re
				nil ;; no limit
				t ;; don't signal error, return nil
				)
	(pcase (s-trim (match-string 0))
          (`"}" (zsh--get-local-variables (1+ open-brace-count)))
          (`"{" (if (> open-brace-count 0)
                    (zsh--get-local-variables (1- open-brace-count))
                  ;; else (found begin of block)
		  (-if-let* ((begin-of-block (match-beginning 0))
			     (line-start (save-excursion
					   (goto-char begin-of-block)
					   ;; https://emacs.stackexchange.com/a/3822
					   (string-to-number (format-mode-line "%l"))))
			     (searchable-lines
			      (zsh--enumerate-lines
			       (buffer-substring-no-properties
				begin-of-block current-point)
			       begin-of-block)))
		      (--map (-second-item it)
			     (s-match-strings-all
			      (rx line-start (* space) "local" (+ space)
                                  (group (eval zsh--variable-name-re)))
			      searchable-lines))))))))))

(defun zsh--get-set-variables ()
  "Get any variales from any assigment (eg SOME_VAR=…).

It's not scope aware.  No local variables (see `zsh--get-local-variables')."
  (let (
        ;; this will filter out the variable name from a set-variable-statement
        (set-variable-statement-re  (rx
                                     line-start
                                     (* blank)
                                     (? "export" (+ blank))
                                     (group  (eval zsh--variable-name-re))
                                     (? ?+)
                                     ?=))
        (lines-before-current-line (buffer-substring (point-min)
                                                     (line-beginning-position)))
        (lines-after-current-line (buffer-substring (line-end-position)
                                                    (point-max))))
    (-flatten
     (-map
      (lambda (lines)
        (--map (-second-item it)
               (s-match-strings-all set-variable-statement-re lines)))
      `(,lines-before-current-line ,lines-after-current-line)))))

(defconst zsh--loop-begin-or-end-group-variable
  (rx (or (sequence line-start
                   (* (any space)) ;; trailing space
                   (or (sequence
                       "for" (+ space)
                       (group (eval zsh--variable-name-re)))
                      (sequence
                       "while" word-end)))
         (sequence word-start "done" word-end)))
  "Matches a loop begin or end.
Used to match a variable name in ⎡for …⎦ using group 1.")

(defun zsh--get-loop-variables (&optional done-count)
  "Get variables for loops (while, for).

Use DONE-COUNT to check that we're not using a variable of a foreign loop
block."
  (let ((done-count (or done-count 0)))
    (save-excursion
      (when (re-search-backward zsh--loop-begin-or-end-group-variable
                                nil ;; no limit
                                t ;; don't signal error, return nil
                                )
        (if (s-equals? (match-string 0)
                       "done")
            (zsh--get-loop-variables (1+ done-count))
          ;; else (matched loop header)
          (if (> done-count 0)
              (zsh--get-loop-variables (1- done-count))
            ;; else (loop header is ours use matched variable name)
            (list (match-string 1))))))))

(defun zsh--get-variables ()
  "Get environment and local variables.

Combines `zsh--get-loop-variables',
`zsh--get-environment-variables'
 and `zsh--get-set-variables' (in that order/priority)."
  (-uniq
   (append (zsh--get-loop-variables)
           (zsh--get-local-variables)
           (zsh--get-environment-variables)
           (zsh--get-set-variables))))

(defun zsh--in-middle-of-variable-name ()
  "Tell if looking at ${some_var:…}.

Do mind that this matches ⎡$⎦ as a variable name may be empty."
  (looking-back (rx "$"
		    (? "{") ;; braced variable
		    (? "=") ;; sh splitting
		    (? (eval zsh--variable-name-re))
		    )
		0))

(defun zsh-completion-at-point ()
  "Complete at point in zsh buffer.

This is added to `completion-at-point-functions'
See https://emacs.stackexchange.com/a/15277."
  (when (zsh--in-zsh)
    (or
     ;; variable name
     (if-let* ((in-variable-name (zsh--in-middle-of-variable-name))
	       (bounds (or (bounds-of-thing-at-point 'word)
			   `(,(point) . ,(point)))) ;; ⎡$█⎦ or ⎡${█⎦ or ⎡${=█⎦
	       (word-start (car bounds))
	       (word-end (cdr bounds))
	       (prefix (buffer-substring-no-properties
			word-start (point)))
	       (candidates (--filter (s-starts-with? prefix it)
				     (zsh--get-variables))))
	 (list word-start word-end
	       candidates
	       :exclusive 'no
	       :company-docsig #'identity
	       ;;:company-doc-buffer (lambda (cand) (TODO))
	       ;;:company-location (lambda (cand) (TODO))
	       )))))


(provide 'zsh-mode)
;;; zsh-mode.el ends here

